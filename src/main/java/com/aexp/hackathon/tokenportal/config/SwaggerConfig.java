package com.aexp.hackathon.tokenportal.config;

import com.fasterxml.classmate.TypeResolver;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket booksApi() {

        //Configure a new rule for mapping Date to String (could possibly create a new model object for clearer usage)
        AlternateTypeRule jodaDateToString = newRule(DateTime.class, String.class);
        AlternateTypeRule localDateToString = newRule(LocalDate.class, String.class);

        return new Docket(DocumentationType.SWAGGER_2).pathMapping("/api")
                //.groupName("Books");
                .alternateTypeRules(jodaDateToString).alternateTypeRules(localDateToString).apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/.*")).build();
    }

    private ApiInfo apiInfo() {

        return new ApiInfoBuilder().title("Hackathon").description("Rest Services available for consumption").build();
    }
}

