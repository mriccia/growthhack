package com.aexp.hackathon.tokenportal.controller;

import com.aexp.hackathon.tokenportal.utils.LoggingUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {

    protected static final Logger log = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    protected LoggingUtility loggingUtility;



}
