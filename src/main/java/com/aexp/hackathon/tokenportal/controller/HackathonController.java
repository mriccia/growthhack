package com.aexp.hackathon.tokenportal.controller;

import com.aexp.hackathon.tokenportal.dao.AccountDao;
import com.aexp.hackathon.tokenportal.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;


@Controller
@CrossOrigin
public class HackathonController extends BaseController {

    @Autowired
    AccountDao accountDao;

    @RequestMapping(value = "/payment", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Payment>> getPayments(@RequestParam String cardId) {
        return null;
    }

    @RequestMapping(value = "/payment", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<List<Payment>> getPayments(@RequestParam String cardId, @RequestBody  List<Payment> payments) {
        return null;
    }


    @RequestMapping(value = "/merchant", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Merchant>> getMerchants() {
        Merchant vipMerchant = new Merchant();
        vipMerchant.setId("MVIP");
        vipMerchant.setLocation("Old Stiene, Brighton");
        vipMerchant.setMerchantName("VIP Pizza");
        vipMerchant.getCategories().add("restaurant");
        vipMerchant.getOffers().add(new Offer("2-for-1 Thursday Lunch", "http://test.com"));
        vipMerchant.getOffers().add(new Offer("Free wine friday", "http://test.com"));

        Merchant healthyLunch = new Merchant();
        healthyLunch.setId("MHealthyLunch");
        healthyLunch.setLocation("Chirchill Square, Brighton");
        healthyLunch.setMerchantName("Healthy Lunch");
        vipMerchant.getCategories().add("restaurant");

        Merchant amazon = new Merchant();
        amazon.setId("Mamazon");
        amazon.setLocation("Online");
        amazon.setMerchantName("Amazon");

        Merchant tesco = new Merchant();
        tesco.setId("MTesco");
        tesco.setLocation("Global");
        tesco.setMerchantName("Tesco");

        Merchant southernRail = new Merchant();
        southernRail.setId("MSouthernRail");
        southernRail.setLocation("Global");
        southernRail.setMerchantName("Southern Rail");
        southernRail.getCategories().add("transport");

        Merchant waterstones = new Merchant();
        waterstones.setId("MWaterstones");
        waterstones.setLocation("Global");
        waterstones.setMerchantName("Waterstones");

        Merchant universityOfSussex = new Merchant();
        universityOfSussex.setId("MuniversityOfSussex");
        universityOfSussex.setLocation("University of Sussex, East Sussex");
        universityOfSussex.setMerchantName("University Of Sussex");

        List<Merchant> result = new LinkedList<Merchant>();
        result.add(vipMerchant);
        result.add(healthyLunch);
        result.add(amazon);
        result.add(tesco);
        result.add(southernRail);
        result.add(waterstones);
        result.add(universityOfSussex);

        return new ResponseEntity<List<Merchant>>(result, HttpStatus.OK);
    }

    /**
     * Get an account with the given id
     * @param id
     * @return Account
     */
    @RequestMapping(value = "/account", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Account> getAccount() {
        List<Account> list = accountDao.list();
        if(list.size() > 0)
        {
            return new ResponseEntity<Account>( list.get(0), HttpStatus.OK);
        }
        Account defaultAccount = new Account();
        defaultAccount.setFirstName("Luke");
        defaultAccount.setLastName("Gardiner");
        return new ResponseEntity<Account>(defaultAccount, HttpStatus.OK);
    }

    /**
     * Get a list of all the accounts
     * @return List<Account>
     */
    @RequestMapping(value = "/accounts", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Account>> list() {
        return new ResponseEntity<List<Account>>(accountDao.list(), HttpStatus.OK);
    }

    /**
     * Save an account
     * @param account
     * @return
     */
    @RequestMapping(value = "/account", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Void> postAccount(@RequestBody Account account) {
        accountDao.saveAccount(account);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    /**
     * Update an existing account
     * @param account
     * @return
     */
    @RequestMapping(value = "/account", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<Void> update(@RequestBody Account account) {
        accountDao.update(account);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
