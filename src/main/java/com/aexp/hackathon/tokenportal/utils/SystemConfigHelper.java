package com.aexp.hackathon.tokenportal.utils;

import org.apache.commons.lang.StringUtils;

public class SystemConfigHelper {

    public static boolean isE3() {
        return "e3".equals(getEnvironment());
    }

    public static String getEnvironment() {
        String environment = System.getProperty("spring.profiles.active");
        if (StringUtils.isEmpty(environment)) {
            environment = "e0";
        }
        return environment;
    }

}
