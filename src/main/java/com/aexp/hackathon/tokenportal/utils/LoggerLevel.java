/*
 * Created on Jul 20, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.aexp.hackathon.tokenportal.utils;

import org.apache.log4j.Level;

public class LoggerLevel extends Level {

    /**
     * @param arg0
     * @param arg1
     * @param arg2
     */
    protected LoggerLevel(int arg0, String arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

}
