package com.aexp.hackathon.tokenportal.utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class LoggingUtility
{
    // Added for Log4j
    private Logger log = Logger.getLogger(com.aexp.hackathon.tokenportal.utils.LoggingUtility.class);
    //private static Logger log1;
    private LoggingUtility loggingUtility =	null;
    //private static GNSAppUtility GNSAPPutility = null;



    public void writeToTraceLog(String text, int tier, long type, Object loggingClass, String loggingMethod)
    {
	 	/*
	 	 * Added the below two lines for logging based on Log4j
	 	 *
	 	 */
        StackTraceElement s[]=Thread.currentThread().getStackTrace();
        log.info(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+text);

    }

    public void writeToCAUnicenter(String alertID, long type, Object loggingClass, String loggingMethod)
    {
        StackTraceElement s[]=Thread.currentThread().getStackTrace();
        log.error(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+alertID);
    }


    public void writeToCAUnicenter(String alertID, Object replaceParams[], long type, Object loggingClass, String loggingMethod)
    {
        StackTraceElement s[]=Thread.currentThread().getStackTrace();
        log.error(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+alertID);
    }

    public void writeToCAUnicenter(String alertID, String replaceParams, long type, String loggingClass, String loggingMethod)
    {

        StackTraceElement s[]=Thread.currentThread().getStackTrace();
        log.error(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+alertID);
    }
    public void writeToCAUnicenter(String alertID, Level level,String replaceParams,  String loggingClass)
    {

        StackTraceElement s[]=Thread.currentThread().getStackTrace();

        log.error(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+"alertID "+":"+alertID );
    }

    public void writeToCAUnicenter(String alertID, Object replaceParams[], long type, Object loggingClass, String loggingMethod,Object obj)
    {
        StackTraceElement s[]=Thread.currentThread().getStackTrace();
        log.error(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+alertID);
    }

    public void writeToCAUnicenterText(String text, long type, Object loggingClass, String loggingMethod)
    {
        StackTraceElement s[]=Thread.currentThread().getStackTrace();
        log.info(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+text);
    }

    public void writeToCAUnicenter(String loggingMethod)
    {
        log.error(loggingMethod);
    }


    public void writeToTraceLog(String identifier, Object obj, int tier, long type, Object loggingClass, String loggingMethod)
    {
        StackTraceElement s[]=Thread.currentThread().getStackTrace();
        log.info(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+identifier);
    }

    public void writeToTraceLog(String identifier,Level level, String loggingClass, String loggingMethod)
    {
        StackTraceElement s[]=Thread.currentThread().getStackTrace();
        log.info(s[3].getClassName()+":"+s[3].getMethodName()+":"+s[3].getLineNumber()+":"+identifier);
    }



}
