package com.aexp.hackathon.tokenportal.dao;

import com.aexp.hackathon.tokenportal.domain.Account;
import com.aexp.hackathon.tokenportal.domain.Card;
import com.aexp.hackathon.tokenportal.domain.Payment;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Repository
public class AccountDao extends BaseMongoRepositoryImpl {

    public Account getById(String id) {
        Account accountFromDb = get(Account.class, id);
        return accountFromDb;
    }

    public List<Account> list() {
        return list(Account.class);
    }

    public void saveAccount(Account accountToSave) {
        save(accountToSave);
    }

    public void updateAccount(Account accountToUpdate) {
        update(accountToUpdate);
    }



    public LinkedList<Payment> getCardPayments(String cardId){
        Card card = listByJsonQuery(Card.class, "{ cards : { id : $eq : " + cardId + " } }");
        return card.getPayments();
    }
}
