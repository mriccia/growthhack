package com.aexp.hackathon.tokenportal.dao;

import com.aexp.hackathon.tokenportal.domain.HackathonPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HackathonDao extends BaseMongoRepositoryImpl {

    public HackathonPojo getById(String id) {
        return get(HackathonPojo.class, id);
    }

    public void save(HackathonPojo pojo) {
        saveOrUpdate(pojo);
    }

    public List<HackathonPojo> list() {
        return list(HackathonPojo.class);
    }

}
