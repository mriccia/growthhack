package com.aexp.hackathon.tokenportal.domain;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Merchant extends BaseMongoEntity{
    String id;
    String merchantName;
    String location;
    List<String> categories;
    LinkedList<Offer> offers;

    public Merchant() {
        offers = new LinkedList<Offer>();
        categories = new ArrayList<String>();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LinkedList<Offer> getOffers() {
        return offers;
    }

    public void setOffers(LinkedList<Offer> offers) {
        this.offers = offers;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }
}
