package com.aexp.hackathon.tokenportal.domain;

public class Offer extends BaseMongoEntity{
    String offerName;
    String offerArtUrl;

    public Offer(String offerName, String offerArtUrl) {
        this.offerName = offerName;
        this.offerArtUrl = offerArtUrl;
    }

    public Offer() {

    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getOfferArtUrl() {
        return offerArtUrl;
    }

    public void setOfferArtUrl(String offerArtUrl) {
        this.offerArtUrl = offerArtUrl;
    }
}
