package com.aexp.hackathon.tokenportal.domain;

public class DailyLimit extends Limit{
    String type = "daily_limit";
    String amount;
    Boolean validMon = false;
    Boolean validTue = false;
    Boolean validWed = false;
    Boolean validThur = false;
    Boolean validFri = false;
    Boolean validSat = false;
    Boolean validSun = false;
    Boolean validAllDays = false;

    public Boolean getValidAllDays() {
        return validAllDays;
    }

    public void setValidAllDays(Boolean validAllDays) {
        this.validAllDays = validAllDays;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Boolean getValidMon() {
        return validMon;
    }

    public void setValidMon(Boolean validMon) {
        this.validMon = validMon;
    }

    public Boolean getValidTue() {
        return validTue;
    }

    public void setValidTue(Boolean validTue) {
        this.validTue = validTue;
    }

    public Boolean getValidWed() {
        return validWed;
    }

    public void setValidWed(Boolean validWed) {
        this.validWed = validWed;
    }

    public Boolean getValidThur() {
        return validThur;
    }

    public void setValidThur(Boolean validThur) {
        this.validThur = validThur;
    }

    public Boolean getValidFri() {
        return validFri;
    }

    public void setValidFri(Boolean validFri) {
        this.validFri = validFri;
    }

    public Boolean getValidSat() {
        return validSat;
    }

    public void setValidSat(Boolean validSat) {
        this.validSat = validSat;
    }

    public Boolean getValidSun() {
        return validSun;
    }

    public void setValidSun(Boolean validSun) {
        this.validSun = validSun;
    }
}
