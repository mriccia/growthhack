package com.aexp.hackathon.tokenportal.domain;

import java.util.LinkedList;

public class Card extends BaseMongoEntity{
    String id;
    String cardName;
    String expr;
    String cvc;
    String cardArtUrl;
    String recipientName;
    LinkedList<Limit> limits;
    LinkedList<Payment> payments;
    LinkedList<Product> products;

    public Card() {
        limits = new LinkedList<Limit>();
        payments = new LinkedList<Payment>();
        products = new LinkedList<Product>();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getExpr() {
        return expr;
    }

    public void setExpr(String expr) {
        this.expr = expr;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String getCardArtUrl() {
        return cardArtUrl;
    }

    public void setCardArtUrl(String cardArtUrl) {
        this.cardArtUrl = cardArtUrl;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public LinkedList<Limit> getLimits() {
        return limits;
    }

    public void setLimits(LinkedList<Limit> limits) {
        this.limits = limits;
    }

    public LinkedList<Payment> getPayments() {
        return payments;
    }

    public void setPayments(LinkedList<Payment> payments) {
        this.payments = payments;
    }

    public LinkedList<Product> getProducts() {
        return products;
    }

    public void setProducts(LinkedList<Product> products) {
        this.products = products;
    }
}
