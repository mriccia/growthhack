package com.aexp.hackathon.tokenportal.domain;

import java.util.LinkedList;

public class Account extends BaseMongoEntity {
    String firstName;
    String lastName;
    LinkedList<Card> cards;

    public Account() {
        cards = new LinkedList<Card>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LinkedList<Card> getCards() {
        return cards;
    }

    public void setCards(LinkedList<Card> cards) {
        this.cards = cards;
    }
}
