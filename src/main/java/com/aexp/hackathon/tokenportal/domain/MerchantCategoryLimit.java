package com.aexp.hackathon.tokenportal.domain;

import java.util.ArrayList;
import java.util.List;

public class MerchantCategoryLimit extends Limit {

    String type = "mcc";
    List<String> allowedCategories;

    public MerchantCategoryLimit() {
        allowedCategories = new ArrayList<>();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getAllowedCategories() {
        return allowedCategories;
    }

    public void setAllowedCategories(List<String> allowedCategories) {
        this.allowedCategories = allowedCategories;
    }
}
