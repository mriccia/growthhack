package com.aexp.hackathon.tokenportal.domain;

public class TotalLimit extends Limit {
    String type = "total_limit";
    String amount;
    Boolean webOnly;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Boolean getWebOnly() {
        return webOnly;
    }

    public void setWebOnly(Boolean webOnly) {
        this.webOnly = webOnly;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
