package com.aexp.hackathon.tokenportal.domain;

/**
 * Created by djhampto on 01/09/16.
 */
public class HackathonPojo extends BaseMongoEntity {

    String firstName, surname;

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }
}
