package com.aexp.hackathon.tokenportal.domain;

import java.util.LinkedList;

public class SpecificMerchantLimit extends Limit{
    String type = "merchant_limit";
    public LinkedList<Merchant> whitelist;
    public LinkedList<Merchant> blacklist;

    public SpecificMerchantLimit() {
        whitelist = new LinkedList<Merchant>();
        blacklist = new LinkedList<Merchant>();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LinkedList<Merchant> getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(LinkedList<Merchant> whitelist) {
        this.whitelist = whitelist;
    }

    public LinkedList<Merchant> getBlacklist() {
        return blacklist;
    }

    public void setBlacklist(LinkedList<Merchant> blacklist) {
        this.blacklist = blacklist;
    }
}
