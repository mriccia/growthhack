package com.aexp.hackathon.tokenportal.domain;

import com.aexp.hackathon.tokenportal.utils.JacksonHelper;
import org.springframework.data.annotation.Id;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public abstract class BaseMongoEntity implements Serializable {

    @Id
    protected String id;
    protected Date created;
    protected Date updated;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public Date getCreated() {

        return created;
    }

    public void setCreated(Date created) {

        this.created = created;
    }

    public Date getUpdated() {

        return updated;
    }

    public void setUpdated(Date updated) {

        this.updated = updated;
    }

    @Override
    public String toString() {

        try {
            return JacksonHelper.convertToJSON(this);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return "??";
    }

    public boolean isPersisted() {

        return id != null;
    }

}
