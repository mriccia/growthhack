package com.aexp.hackathon.tokenportal.domain;

public class TransactionNumberLimit extends Limit {
    String type = "num_transactions";
    int transaction;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTransaction() {
        return transaction;
    }

    public void setTransaction(int transaction) {
        this.transaction = transaction;
    }
}
