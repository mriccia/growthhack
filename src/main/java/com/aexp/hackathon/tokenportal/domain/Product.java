package com.aexp.hackathon.tokenportal.domain;

public class Product {
    String name;
    Boolean selected;

    public Product() {

    }

    public Product(String name, Boolean selected) {
        this.name = name;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
