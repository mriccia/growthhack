angular.module('growthHacka',["chart.js"]);

var appa = angular.module('Growth', ['ui.router', 'growthHacka','ui.bootstrap', 'ui.select', 'ngSanitize', 'chart.js']);

appa.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/dashboard");

    $stateProvider
        .state('applePay', {
            url: "/apple-pay/:id",
            templateUrl: "features/apple-pay/card-image.html",
            controller: 'CardController',
            resolve: {
              card: ['$stateParams', 'CardService', function($stateParams, CardService){
                return CardService.getCard($stateParams.id);
              }]
            }
        })
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "features/dashboard/dashboard.html",
            controller: "DashboardCtrl"
        })
        .state('dashboard.reporting', {
            url: "/:id",
            templateUrl: "features/reporting/reporting.html",
            controller: "ReportCtrl",
            resolve: {
                card: ['$stateParams', 'CardService', function($stateParams, CardService){
                    return CardService.getCard($stateParams.id);
                }]
            }
        })
        .state('addCard', {
            url: "/card/new",
            templateUrl: "features/cards/add-card.html",
            controller: "AddCardCtrl"
        })
        .state('updateCard', {
            url: "/card/:id",
            templateUrl: "features/cards/add-card.html",
            controller: "EditCardCtrl",
            resolve: {
                card: ['$stateParams', 'CardService', function($stateParams, CardService){
                    return CardService.getCard($stateParams.id);
                }]
            }
        })
        .state('reporting', {
            url: "/reporting/:id",
            templateUrl: "features/reporting/reporting.html",
            controller: "ReportCtrl",
            resolve: {
                card: ['$stateParams', 'CardService', function($stateParams, CardService){
                    return CardService.getCard($stateParams.id);
                }]
            }
        })
}]).run( ['$rootScope', '$state', '$stateParams',
    function ($rootScope,   $state,   $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }]);