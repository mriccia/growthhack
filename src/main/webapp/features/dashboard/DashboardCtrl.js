angular.module('growthHacka')
  .controller('DashboardCtrl', ['$scope', 'CardService', function ($scope, CardService) {
      CardService.getCards().then(function (person) {
        $scope.person = person;
        $scope.totalBalance = person.cards.map(function (el) {
          return el.balance;
        }).reduce(add, 0);
        $scope.averageMonthly = person.cards.map(function (el) {
          return el.payments || [];
        }).reduce(concatArrays, [])
          .reduce(transactionsByMonths, {});
      });
      $scope.expand = false;
      $scope.selectedCard = {};
      function add(prev, curr) {
        return prev + curr;
      }
      function transactionsByMonths(prev, curr) {
        var monthOfTransaction = moment.unix(curr.time/1000).format("MMM-YYYY");
        if(typeof prev[monthOfTransaction] === 'undefined'){
          prev[monthOfTransaction] = [curr];
        }else{
          prev[monthOfTransaction] = prev[monthOfTransaction].concat(curr);
        }
        return prev;
      }
      function concatArrays(prev, curr){
        return prev.concat(curr);
      }
    }
    ]
  );