angular.module('growthHacka')
  .controller("ReportCtrl", ['card', '$scope', '$filter', function (card, $scope, $filter) {
    $scope.card = card;
    $scope.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    $scope.series = ['Money Spent'];

    $scope.data = [
      [65, 59, 80, 81, 56, 55, 40],
      [28, 48, 40, 19, 86, 27, 90]
    ];

    $scope.amounts = [];
    $scope.amountsPerDay = [];
    if (card.payments) {
      var amounts = card.payments.map(function (payment) {
        return payment.amount;
      });

      $scope.categories = [];
      var categories = card.payments.map(function (payment) {
        return payment.merchantCategory;
      });

      $scope.days = [];
      var days = card.payments.map(function (payment) {
        return $filter('date')(payment.time, "yyyy-MM");
      });

      var merchants = card.payments.map(function (payment) {
        return payment.merchantName;
      });

      $scope.merchants = [];
      $scope.transactions = [];

      for (var i = 0; i < categories.length; i++) {
        var index = $scope.categories.indexOf(categories[i]);
        var index2 = $scope.merchants.indexOf(merchants[i]);
        var index3 = $scope.days.indexOf(days[i]);
        if (index == -1) {
          $scope.categories.push(categories[i]);
          $scope.amounts.push(amounts[i]);
        }
        if (index != -1) {
          $scope.amounts[index] = $scope.amounts[index] + amounts[i];
        }
        if (index2 == -1) {
          $scope.merchants.push(merchants[i]);
          $scope.transactions.push(0);
        }
        if (index2 != -1) {
          $scope.transactions[index2] = $scope.transactions[index2] + 1;
        }
        if (index3 == -1) {
          $scope.days.push(days[i]);
          $scope.amountsPerDay.push(amounts[i]);
        }
        if (index3 != -1) {
          $scope.amountsPerDay[index3] = $scope.amountsPerDay[index3] + amounts[i];
        }
      }
      $scope.days = $scope.days.sort(function (prev, curr) {
        return (moment(prev) < moment(curr)) ? -1 : 1;
      });
      $scope.options = {
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(0,0,0)'
          }
        }
      };
    }
  }]);