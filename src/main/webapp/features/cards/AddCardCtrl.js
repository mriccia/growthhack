angular.module('growthHacka')
    .controller('AddCardCtrl', ['$scope', 'CardService', '$state', function ($scope, CardService, $state) {
        $scope.limitTypes = [
            {
                id: "category",
                name: "Merchant Categories"
            },
            {
                id: "daily_limit",
                name: "Daily spend"
            },
            {
                id: "total_limit",
                name: "Total spend"
            },
            {
                id: "merchant_limit",
                name: "Merchants"
            },
            {
                id: "num_transactions",
                name: "N. of transactions"
            }
        ];
        $scope.merchantCategories = [
            {
                mccCode: 'restaurant',
                name: 'Restaurants'
            },
            {
                mccCode: 'transport',
                name: 'Transport'
            },
            {
                mccCode: 'casino',
                name: 'Casino'
            },
            {
                mccCode: 'sport',
                name: 'Sport'
            },
            {
                mccCode: 'museums',
                name: 'Museums'
            }
        ];
        $scope.merchants = [
            {
                id: 'vip',
                merchantName: 'Very Italian Pizza'
            },
            {
                id: 'h&h',
                merchantName: 'Hare & hounds'
            },
            {
                id: 'stad',
                merchantName: 'Amex Stadium'
            },
            {
                id: 'easy',
                merchantName: 'Easyjet'
            }
        ];
        $scope.cardData = {
            limits: []
        };

        $scope.addCard = function addCard(){
            $scope.cardData.expr = moment($scope.cardData.expr).format("MMM-YYYY");
            CardService.addCard($scope.cardData);
            $state.go('dashboard');
        }
    }]
);