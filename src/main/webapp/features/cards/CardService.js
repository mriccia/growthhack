angular.module('growthHacka')
    .service('CardService', ['$http','$q', function CardService($http, $q) {
        var cards;
        var person = {};
        var personPromise = $http.get("/mockdata.json").then(function(response){
            person = response.data;
            return $q.resolve(person);
        });

        this.getCards = function () {
            return personPromise;
        };

        this.addCard = function addCard(card){
            return personPromise.then(function(person) {
                person.cards.push(card);
                return $q.resolve();
            });
        };

        this.getCard = function getCard(cardIndex){
            return personPromise.then(function(person){
                return $q.resolve(person.cards[cardIndex]);
            });
        };
        this.updateCard = function updateCard(card, index){
            return personPromise.then(function(person) {
                person.cards[index] = card;
                return $q.resolve();
            });
        }
    }]
);
